import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:url_launcher/url_launcher.dart';
export 'package:url_launcher/url_launcher.dart';

class TapIcon extends StatelessWidget {
  final String assetName;
  final String profileUrl;
  const TapIcon({
    required this.profileUrl,
    required this.assetName,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        var url = profileUrl;
        if (await canLaunch(url)) {
          await launch(url, forceSafariVC: false);
        } else {
          throw "couldn't launch $url";
        }
      },
      child: SvgPicture.asset(
        assetName,
        height: 24,
        width: 24,
      ),
    );
  }
}
