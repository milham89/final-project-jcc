import 'package:covidappjcc/cubit/auth_cubit.dart';
import 'package:covidappjcc/elemets/tap_icon.dart';
import 'package:covidappjcc/shared/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AccountPage extends StatelessWidget {
  const AccountPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthCubit, AuthState>(
      builder: (context, state) {
        if (state is AuthSuccess) {
          return Scaffold(
            backgroundColor: kBackgroundColor,
            body: SafeArea(
              child: ListView(
                padding: EdgeInsets.symmetric(
                  horizontal: defaultMargin,
                ),
                children: [
                  home(),
                  name(),
                  desc(),
                  title(),
                  SizedBox(
                    height: 5,
                  ),
                  detailCard(),
                  contact(),
                  SizedBox(
                    height: 10,
                  ),
                  profileCard(),
                ],
              ),
            ),
          );
        } else {
          return SizedBox();
        }
      },
    );
  }
}

Widget title() {
  return Container(
    margin: EdgeInsets.only(top: 20),
    child: Text(
      'Personal Details',
      style: greyTextStyle.copyWith(
        fontSize: 20,
        fontWeight: semiBold,
      ),
    ),
  );
}

Widget contact() {
  return Container(
    margin: EdgeInsets.only(top: 5),
    child: Text(
      'Contact Me',
      style: blackTextStyle.copyWith(
        fontSize: 20,
        fontWeight: semiBold,
      ),
    ),
  );
}

Widget home() {
  return Container(
    margin: EdgeInsets.only(top: 30),
    child: Container(
      width: 150,
      height: 200,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        image: DecorationImage(
          image: NetworkImage(
            'https://scontent.fcgk8-1.fna.fbcdn.net/v/t1.6435-9/80331524_2748504775170788_1300136739947413504_n.jpg?_nc_cat=100&ccb=1-5&_nc_sid=09cbfe&_nc_eui2=AeF7vjJLOg2GINMhgbu7yVTdite40p_RD-aK17jSn9EP5lS_xaR4liQYqPeiJs_qSomZp1rQ1CpvlPSUGfhjS0F1&_nc_ohc=JFuf4KYWL3UAX87bics&_nc_ht=scontent.fcgk8-1.fna&oh=c4d214d5d3a226ead74662ee306a1ba0&oe=6164DAEF',
          ),
        ),
      ),
    ),
  );
}

Widget name() {
  return Container(
    margin: EdgeInsets.only(top: 20, left: 105),
    child: Text(
      'Muhammad Ilham Solehudin',
      style: blackTextStyle.copyWith(
        fontSize: 20,
        fontWeight: semiBold,
      ),
    ),
  );
}

Widget desc() {
  return Container(
    margin: EdgeInsets.only(top: 5, left: 160),
    child: Text(
      'Flutter Developer',
      style: blackTextStyle.copyWith(
        fontSize: 20,
        fontWeight: semiBold,
      ),
    ),
  );
}

Widget detailCard() {
  return Padding(
    padding: EdgeInsets.all(10.0),
    child: Card(
      child: Column(
        children: [
          ListTile(
            leading: SvgPicture.asset(
              'assets/img/phone.svg',
              height: 24,
              width: 15,
            ),
            title: Text(
              '082120273090',
              style: blackTextStyle.copyWith(
                fontSize: 18,
                fontWeight: light,
              ),
            ),
          ),
          Divider(
            height: 10,
            color: kBlackColor,
          ),
          ListTile(
            leading: SvgPicture.asset(
              'assets/img/email.svg',
              height: 24,
              width: 15,
            ),
            title: Text(
              'muhammad_ilham99@ymail.com',
              style: blackTextStyle.copyWith(
                fontSize: 16,
                fontWeight: light,
              ),
            ),
          ),
          Divider(
            height: 10,
            color: kBlackColor,
          ),
          ListTile(
            leading: SvgPicture.asset(
              'assets/img/home.svg',
              height: 24,
              width: 15,
            ),
            title: Text(
              'Kota Bandung',
              style: blackTextStyle.copyWith(
                fontSize: 18,
                fontWeight: light,
              ),
            ),
          ),
        ],
      ),
    ),
  );
}

Widget profileCard() {
  return Stack(
    children: [
      Container(
        margin: EdgeInsets.symmetric(horizontal: 16),
        padding: EdgeInsets.symmetric(vertical: 30, horizontal: 24),
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(17),
          color: Color.fromRGBO(92, 64, 204, 1),
        ),
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: 1, right: 200),
              child: Text(
                '@m_iilhaam',
                style: whiteTextStyle.copyWith(
                  fontSize: 14,
                  fontWeight: semiBold,
                ),
              ),
            ),
            SizedBox(
              height: 8,
            ),
            Container(
              margin: EdgeInsets.only(top: 1, right: 195),
              height: 24,
              child: Text(
                '082120273090',
                style: whiteTextStyle.copyWith(
                  fontSize: 14,
                  fontWeight: semiBold,
                ),
              ),
            ),
            SizedBox(
              height: 8,
            ),
            Container(
              margin: EdgeInsets.only(top: 1, right: 195),
              height: 24,
              child: Text(
                '@SouLoNeLy_',
                style: whiteTextStyle.copyWith(
                  fontSize: 14,
                  fontWeight: semiBold,
                ),
              ),
            ),
            SizedBox(
              height: 8,
            ),
            Container(
              margin: EdgeInsets.only(top: 1, right: 215),
              height: 24,
              child: Text(
                'milham89',
                style: whiteTextStyle.copyWith(
                  fontSize: 14,
                  fontWeight: semiBold,
                ),
              ),
            ),
            SizedBox(
              height: 8,
            ),
            Container(
              margin: EdgeInsets.only(top: 1, right: 140),
              height: 24,
              child: Text(
                'muhammad-ilham-s',
                style: whiteTextStyle.copyWith(
                  fontSize: 14,
                  fontWeight: semiBold,
                ),
              ),
            ),
          ],
        ),
      ),
      Container(
        margin: EdgeInsets.symmetric(horizontal: 16),
        padding: EdgeInsets.symmetric(vertical: 30, horizontal: 24),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(17),
          color: Color.fromRGBO(113, 81, 241, 1),
        ),
        child: Column(
          children: [
            TapIcon(
              profileUrl: 'https://www.instagram.com/m_iilhaamm/',
              assetName: 'assets/img/instagram.svg',
            ),
            SizedBox(
              height: 8,
            ),
            TapIcon(
              profileUrl: 'https://wa.me/6282120273090',
              assetName: 'assets/img/whatsapp.svg',
            ),
            SizedBox(
              height: 8,
            ),
            TapIcon(
              profileUrl: 'https://twitter.com/SouLoNeLy_',
              assetName: 'assets/img/twitter.svg',
            ),
            SizedBox(
              height: 8,
            ),
            TapIcon(
              profileUrl: 'https://github.com/milham89',
              assetName: 'assets/img/github.svg',
            ),
            SizedBox(
              height: 8,
            ),
            TapIcon(
              profileUrl: 'https://www.linkedin.com/in/muhammad-ilham-s/',
              assetName: 'assets/img/linkedin.svg',
            ),
          ],
        ),
      ),
    ],
  );
}
